<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>sovos-all-tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ce9e8dc4-dbd8-4ba9-99e4-0ad09980fc4f</testSuiteGuid>
   <testCaseLink>
      <guid>f2f16d64-5f2c-4710-94ad-619435ed44f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc008-ValidateDeleteUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92556455-1c84-450a-82b5-f2d6a6c5f352</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc007-ValidateUpdateUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f445da5d-dd55-4921-847d-80a099e91909</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc006-ValidateGetNewUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f81e65dd-806d-4050-873c-dcda2a573459</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc005-ValidateGetUnregisteredUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab1d265e-69d0-49a2-916c-91fa02d46439</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc004-ValidateGetUserByID</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e09bce2d-074c-4e85-be38-084d7b2f4397</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc003-ValidateGetUsers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aec139bf-d20d-4edf-bdcb-9e7d99e6761c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc002-ValidateRegisterUserDuplicated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e17d419-3462-48e6-8c4f-ff21779936b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/APITests/tc001-ValidateRegisterNewUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fba1cd85-f0dc-44a5-933d-2a15b314a2d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UITests/tc004-ValidateSearchSpecialCharacter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e2805e5-d63a-449d-a2d7-f102c84e3192</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UITests/tc003-ValidateDepartments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eed13c43-a945-4787-a74d-079d32367899</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UITests/tc002-ValidateSearchNoResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>759a24aa-d1c1-4af0-8600-bf7561409fb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UITests/tc001-ValidateSearchSuccess</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
