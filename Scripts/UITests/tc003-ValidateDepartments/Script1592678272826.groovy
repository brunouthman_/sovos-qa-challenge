import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.waitForPageLoad(5)

//validate department1
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department1, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

String resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart1, resultSearchDepartmentTitle)

//validate department2
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department2, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart2, resultSearchDepartmentTitle)

//validate department3
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department3, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart3, resultSearchDepartmentTitle)

//validate department4
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department4, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart4, resultSearchDepartmentTitle)

//validate department5
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department5, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart5, resultSearchDepartmentTitle)

//validate department6
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department6, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart6, resultSearchDepartmentTitle)

//validate department7
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department7, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart7, resultSearchDepartmentTitle)


//validate department8
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department8, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart8, resultSearchDepartmentTitle)

//validate department9
WebUI.selectOptionByValue(findTestObject('UITests/PageAmazonSearch/searchDropdown'), GlobalVariable.department9, false)

WebUI.sendKeys(findTestObject('UITests/PageAmazonSearch/inputSearch'), Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

resultSearchDepartmentTitle = WebUI.getText(findTestObject('UITests/PageAmazonResults/resultSearchDepartmentTitle'))

println(resultSearchDepartmentTitle)

WebUI.verifyEqual(GlobalVariable.titleDepart9, resultSearchDepartmentTitle)

//27 departments... this is a test, then just 9 here  

WebUI.closeBrowser()

